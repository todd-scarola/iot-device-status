'use strict';
const AWS = require('aws-sdk');
const DDB = new AWS.DynamoDB();

/*
{
  "messageType": "gatewayStatus",
  "gatewayId": "354616090318416",
  "eventTime": "2020-11-30T18:48:08.000Z",
  "firmware": 197961,
  "rssi": -100,
  "sinr": 9
}
*/

exports.updateGatewayStatus = async(status) => {
   try {

      //strip off monites and seconds so we only record 1 status update per hour:
      var d = new Date(status.eventTime);
      d.setMinutes(0);
      d.setSeconds(0);
      status.eventTime = d;

      //set TTL for dynamo cleanup
      var ttl = new Date(d);
      ttl.setDate(ttl.getDate() + 180);
      ttl = ttl.getTime();

      var params = {
         TableName: process.env.GATEWAY_STATUS_TABLE,
         Key: {
            gatewayId: { S: status.gatewayId },
            eventTime: { S: new Date(status.eventTime).toISOString() }
         },
         UpdateExpression: 'SET firmware=:firmware, rssi=:rssi, sinr=:sinr, expires=:expires, updateCount=updateCount+:inc',
         ExpressionAttributeValues: {
            ":firmware": { "S": status.firmware.toString() },
            ":rssi": { "N": status.rssi.toString() },
            ":sinr": { "N": status.sinr.toString() },
            ":expires": { "N": ttl.toString() },
            ':inc': { 'N': '1' }
         },
         ReturnValues: "ALL_NEW"
      };

      try {
         //update
         return await DDB.updateItem(params).promise();
      }
      catch (err) {
         //updateCount doesn't yet exist
         params.UpdateExpression = params.UpdateExpression.replace("updateCount+:inc", ":inc");
         return await DDB.updateItem(params).promise();
      }
   }
   catch (err) {
      console.log('ERROR processing updateGatewayStatus message: %j', status);
   }
};

exports.updateGateway = async(status) => {
   try {

      var params = {
         TableName: process.env.GATEWAY_TABLE,
         Key: {
            gatewayId: { S: status.gatewayId },
         },
         UpdateExpression: 'SET firmware=:firmware, rssi=:rssi, sinr=:sinr, lastStatusUpdateAt=:lastStatusUpdateAt, updatedAt=:updatedAt, updateCount=updateCount+:inc',
         ExpressionAttributeValues: {
            ":firmware": { "S": status.firmware.toString() },
            ":rssi": { "N": status.rssi.toString() },
            ":sinr": { "N": status.sinr.toString() },
            ":lastStatusUpdateAt": { "S": new Date(status.eventTime).toISOString() },
            ":updatedAt": { "S": new Date().toISOString() },
            ':inc': { 'N': '1' }
         },
         ReturnValues: "ALL_NEW"
      };

      try {
         //update
         return await DDB.updateItem(params).promise();
      }
      catch (err) {
         //updateCount doesn't yet exist
         params.UpdateExpression = params.UpdateExpression.replace("updateCount+:inc", ":inc");
         return await DDB.updateItem(params).promise();
      }
   }
   catch (err) {
      console.log('ERROR processing updateGateway message: %j', status);
   }
};


/*
{
  "messageType": "tagStatus",
  "gateway_id": "354616090305769",
  "tag_id": "c139864a77a2",
  "battery_level": 2816,
  "battery_percent": 40,
  "firmware": "00030000",
  "device_time": 1606762095,
  "upload_time": 1606762076,
  "previous_upload_time": 1606761965,
  "received_time": 1606762090548
}
*/

exports.updateTagStatus = async(status) => {
   try {
      //strip off monites and seconds so we only record 1 status update per hour:
      var d = new Date(Number(status.device_time) * 1000);
      d.setMinutes(0);
      d.setSeconds(0);
      status.device_time = d;

      //set TTL for dynamo cleanup
      var ttl = new Date(d);
      ttl.setDate(ttl.getDate() + 180);
      ttl = ttl.getTime();

      var params = {
         TableName: process.env.TAG_STATUS_TABLE,
         Key: {
            tagId: { S: status.tag_id },
            eventTime: { S: new Date(status.device_time).toISOString() }
         },
         UpdateExpression: 'SET gatewayId=:gatewayId, batteryLevel=:batteryLevel,batteryPercent=:batteryPercent, \
      firmware=:firmware, receivedTime=:receivedTime, uploadTime=:uploadTime, previousUploadTime=:previousUploadTime, expires=:expires, updateCount=updateCount+:inc',
         ExpressionAttributeValues: {
            ":gatewayId": { "S": status.gateway_id },
            ":batteryLevel": { "N": status.battery_level.toString() },
            ":batteryPercent": { "N": status.battery_percent.toString() },
            ":firmware": { "S": status.firmware.toString() },
            ":receivedTime": { "S": new Date(status.received_time).toISOString() },
            ":uploadTime": { "S": new Date(Number(status.upload_time) * 1000).toISOString() },
            ":previousUploadTime": { "S": new Date(Number(status.previous_upload_time) * 1000).toISOString() },
            ":expires": { "N": ttl.toString() },
            ':inc': { 'N': '1' }
         },
         ReturnValues: "ALL_NEW"
      };

      try {
         //update
         return await DDB.updateItem(params).promise();
      }
      catch (err) {
         //updateCount doesn't yet exist
         params.UpdateExpression = params.UpdateExpression.replace("updateCount+:inc", ":inc");
         return await DDB.updateItem(params).promise();
      }
   }
   catch (err) {
      console.log('ERROR processing updateTagStatus message: %j', status);
   }

};

exports.updateTag = async(status) => {
   try {
      var params = {
         TableName: process.env.TAG_TABLE,
         Key: {
            tagId: { S: status.tag_id }
         },
         UpdateExpression: 'SET gatewayId=:gatewayId, batteryLevel=:batteryLevel, batteryPercent=:batteryPercent, \
      firmware=:firmware, receivedTime=:receivedTime, uploadTime=:uploadTime, previousUploadTime=:previousUploadTime, deviceTime=:deviceTime, updatedAt=:updatedAt, updateCount=updateCount+:inc',
         ExpressionAttributeValues: {
            ":gatewayId": { "S": status.gateway_id },
            ":batteryLevel": { "N": status.battery_level.toString() },
            ":batteryPercent": { "N": status.battery_percent.toString() },
            ":firmware": { "S": status.firmware.toString() },
            ":receivedTime": { "S": new Date(status.received_time).toISOString() },
            ":uploadTime": { "S": new Date(Number(status.upload_time) * 1000).toISOString() },
            ":previousUploadTime": { "S": new Date(Number(status.previous_upload_time) * 1000).toISOString() },
            ":deviceTime": { "S": new Date(Number(status.device_time) * 1000).toISOString() },
            ":updatedAt": { "S": new Date().toISOString() },
            ':inc': { 'N': '1' },
         },
         //ConditionExpression: "uploadTime < :uploadTime",
         ReturnValues: "ALL_NEW"
      };

      //TO DO: Need to add conditional update so that older tag updates don't overwrite this value

      try {
         //update
         return await DDB.updateItem(params).promise();
      }
      catch (err) {

         if (err == 'ConditionalCheckFailedException: The conditional request failed') {
            console.log('update ignored: %s', err);
         }
         else if (err == 'ValidationException: The provided expression refers to an attribute that does not exist in the item') {
            params.UpdateExpression = params.UpdateExpression.replace("updateCount+:inc", ":inc");
            return await DDB.updateItem(params).promise();
         }
         else {
            console.log('error:%s', err);
         }
      }
   }
   catch (err) {
      console.log('ERROR processing updateTag message: %s', err);
   }
};


exports.updateGatewayUpload = async(status) => {
   try {
      var update_expression = 'SET ';
      update_expression += 'lastTagUploadAt=:lastTagUploadAt,';
      update_expression += 'updatedAt=:updatedAt';

      var params = {
         TableName: process.env.GATEWAY_TABLE,
         Key: {
            gatewayId: { S: status.gateway_id }
         },
         UpdateExpression: update_expression,
         ExpressionAttributeValues: {
            ":lastTagUploadAt": { "S": new Date(Number(status.upload_time) * 1000).toISOString() },
            ":updatedAt": { "S": new Date().toISOString() }
         },
         ReturnValues: 'ALL_NEW'
      };

      return await DDB.updateItem(params).promise();
   }
   catch (err) {
      console.log('ERROR processing updateGatewayUpload message: %j', status);
   }
};




/*
{
  "messageType": "tagContact",
  "gateway_id": "354616090312245",
  "tag_a": "cd71770dac99",
  "tag_b": "e62c2d5babb8",
  "event_start": 1608570155,
  "event_end": 1608570215,
  "range": 2,
  "duration": 60,
  "meta": {}
}
*/


exports.updateTagContact = async(event) => {
   try {
      var update_expression = 'SET ';
      update_expression += 'eventEnd=:eventEnd,';
      update_expression += 'tagA=:tagA,';
      update_expression += 'tagB=:tagB,';
      update_expression += 'distance=:distance,';
      update_expression += 'gatewayId=:gatewayId,';
      update_expression += 'updatedAt=:updatedAt';

      var params = {
         TableName: process.env.TAG_CONTACT_TABLE,
         Key: {
            contactId: { S: event.tag_a.concat('-', event.tag_b) },
            eventStart: { S: new Date(Number(event.event_start * 1000)).toISOString() }
         },
         UpdateExpression: update_expression,
         ExpressionAttributeValues: {
            ":eventEnd": { "S": new Date(Number(event.event_end * 1000)).toISOString() },
            ":tagA": { "S": event.tag_a },
            ":tagB": { "S": event.tag_b },
            ":distance": { "N": event.range.toString() },
            ":gatewayId": { "S": event.gateway_id },
            ":updatedAt": { "S": new Date().toISOString() }

         },
         ReturnValues: 'ALL_NEW'
      };

      return await DDB.updateItem(params).promise();
   }
   catch (err) {
      console.log('ERROR processing updateTagContact message: %j', event);
   }
};


exports.updateTagB = async(event) => {
   try {
      var params = {
         TableName: process.env.TAG_TABLE,
         Key: {
            tagId: { S: event.tag_b }
         },
         UpdateExpression: 'SET detectedTime=:detectedTime,updatedAt=:updatedAt',
         ExpressionAttributeValues: {
            ":detectedTime": { "S": new Date(Number(event.event_end) * 1000).toISOString() },
            ":updatedAt": { "S": new Date().toISOString() }
         },
         ReturnValues: "ALL_NEW"
      };

      return await DDB.updateItem(params).promise();
   }
   catch (err) {
      console.log('ERROR processing updateTagB message: %j', err);
   }

};
