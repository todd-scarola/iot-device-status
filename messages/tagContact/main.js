'use strict';
const DDB = require('../../sharedDynamo.js');

//main BL for aggregating a contact into an exposure
exports.processData = async(event) => {

  //update tagA data
   await DDB.updateTagContact(event);

  //update lastDetectedAt for tagB data
  await DDB.updateTagB(event);

   return event;

};

/*
{
  "messageType": "tagContact",
  "gateway_id": "354616090312245",
  "tag_a": "cd71770dac99",
  "tag_b": "e62c2d5babb8",
  "event_start": 1608570155,
  "event_end": 1608570215,
  "range": 2,
  "duration": 60,
  "meta": {
    "tag_a": {
      "id": "2998",
      "mac_address": "cd71770dac99",
      "account_id": "40",
      "manufacturer": "Laird",
      "model": "BT510",
      "nickname": "",
      "box_number": "56"
    },
    "tag_b": {
      "id": "5812",
      "mac_address": "e62c2d5babb8",
      "account_id": "40",
      "manufacturer": "Laird",
      "model": "BT510",
      "nickname": null,
      "box_number": null
    },
    "user_a": {
      "user_id": "2939",
      "user_type": "User",
      "checked_in_at": "2020-11-09T17:21:05.317Z",
      "checked_out_at": null
    },
    "user_b": {
      "user_id": "5064",
      "user_type": "User",
      "checked_in_at": "2020-12-16T17:52:34.346Z",
      "checked_out_at": null
    }
  }
}
*/
