'use strict';
//const PG = require('../../sharedPostgres.js');
const DDB = require('../../sharedDynamo.js');

exports.processData = async(event) => {

  //update list of known tags
  await DDB.updateTag(event);

  //updated tag status history
  await DDB.updateTagStatus(event);
  
  //update gateway with last tag contact detected
  await DDB.updateGatewayUpload(event);

  return event;

};

/*
{
  "messageType": "tagStatus",
  "gateway_id": "354616090305769",
  "tag_id": "c139864a77a2",
  "battery_level": 2816,
  "battery_percent": 40,
  "firmware": "00030000",
  "device_time": 1606762095,
  "upload_time": 1606762076,
  "previous_upload_time": 1606761965,
  "received_time": 1606762090548
}
*/
