'use strict';
const DDB = require('../../sharedDynamo.js');

exports.processData = async(event) => {

  //return await PG.addGatewayStatus(event);

  await DDB.updateGateway(event);
  
  await DDB.updateGatewayStatus(event);
  
  return event;
};

/*
{
  "messageType": "gatewayStatus",
  "gatewayId": "354616090318416",
  "eventTime": "2020-11-30T18:48:08.000Z",
  "firmware": 197961,
  "rssi": -100,
  "sinr": 9
}
*/
